// Клиентская библиотека
import com.db4o.*;
import java.util.List;
import java.util.function.Predicate;

class Person
{
    public Person()
    { }
    public Person(String firstName, Integer number){ 
        this.firstName = firstName; 
        this.number = number;
    }
    public String getFirstName() { return firstName; }
    public void setFirstName(String value) { firstName = value; }

    public Integer getNumber() { return number; }
    public void setNumber(Integer value) { number = value; }
    

    public String toString()
    {
        return 
            "[Person: " +
            "firstName = " + firstName + " " +
            "number = " + number + " " +
            "]";
    }
    
    public boolean equals(Object rhs)
    {
        if (rhs == this)
            return true;
        
        if (!(rhs instanceof Person))
            return false;
        
        Person other = (Person)rhs;
        return (this.firstName.equals(other.firstName) && this.number.equals(other.number));
    }
    
    private String firstName;
    private Integer number;
}

class ExampleApp {
    public static void main(String[] args) throws Exception {
	// Данные, которые будут храниться в БД    
        Person personII = new Person("Ivan I", 1);
        Person personIP = new Person("Ivan P", 2);
        Person personPI = new Person("Petr I", 2);
        ObjectContainer db = null;
        try
        {
            // Подключаемся к базе данных в файле
            db = Db4o.openFile("persons.data");
            System.out.println("Connected!");

            // Сохранение данных в БД
            db.store(personII);
            db.store(personIP);
            db.store(personPI);
            System.out.println("Data stored successfuly!");
            // Завершение тразнакции
            db.commit();

            // Поиск объектов в БД по заданному классу
            List<Person> search1 = db.query(Person.class);
            System.out.println("Found " + search1.size() + " objects of Person class");

            // Поиск объектов по образцу
            // За образец берутся все поля объекта, у которых значение отлично от значения по умолчанию
            Person example = new Person();
            example.setNumber(2);
            final ObjectSet<Person> search2 = db.queryByExample(example);
            System.out.println("Found " + search2.size() + " by \"number=2\" example object");

            // Обновление данных
            search1.get(0).setNumber(search1.get(0).getNumber() +1);
            db.store(search1.get(0));
            db.commit();
            System.out.println("Stored Person[0] with number++");
            // Проверяем
            List<Person> persons = db.query(Person.class);
            Integer new_number = persons.get(0).getNumber();
            System.out.println("Person[0].number = " + new_number);

            // Удаление объекта
            db.delete(persons.get(0));
            db.commit();
            System.out.println("Deleted Person[0] from db");
            // Проверяем
            search1 = db.query(Person.class);
            System.out.println("Found " + search1.size() + " objects of Person class");
            
          
        }
        finally
        {
            if (db != null)
                db.close();
        }
    
        System.out.println("Hello World!");
    }
}
