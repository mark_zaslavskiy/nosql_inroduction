# -*- coding: utf-8 -*-
from pymongo import MongoClient
from bson.json_util import dumps # Конвертация из формата bson в json

# Подключение
connection = MongoClient('localhost', 27017)
#print("Connection succed!")

# Создание БД и коллекции
db = connection['no_sql_introductionqqqq']
collection = db['test_collection33333']

# Запис данных
collection.remove()
collection.insert({"name": "lecture1", "description":"introduction"})
collection.insert({"name": "lecture2", "participants": ["student1", "student2"]})
collection.insert({"name": "lecture3", "participants": "student1"})

# Поиск данных
results = collection.find({"participants":"student1"}, {"name":1})
print(dumps(results))
