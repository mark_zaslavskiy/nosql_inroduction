# -*- coding: utf-8 -*-
from cassandra.cluster import Cluster
from cassandra.query import SimpleStatement
from cassandra import ConsistencyLevel


# Задаем набор серверов Cassandra
cluster = Cluster(
    ['127.0.0.1'],
    port=9042)
# Устанавливаем соединение
session = cluster.connect()
# Выбираем существующий keyspace
session.set_keyspace('leti_students');
# Запрашиваем данные
rows = session.execute('SELECT * FROM humanities')
for row in rows:
    print row.student, row.art, row.music
# Запрос с настраиваемым ConsistencyLevel
query = SimpleStatement(
    "INSERT INTO humanities (student, music) VALUES (%s, %s)",
    consistency_level=ConsistencyLevel.ANY)
session.execute(query, ('Sidorov V.V. gr3', 1))
# Подстановка параметров в запрос
session.execute(
    """
    INSERT INTO humanities (student, music)
    VALUES (%(student)s,  %(music)s)
    """,
    {'student': "Petrov B.B. gr2", 'music': 2})
