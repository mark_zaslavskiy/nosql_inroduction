#include <sys/types.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <db.h>

int main(int argc, char** argv){

  DB * mydb;                                                                
  u_int32_t open_flags = DB_RDONLY;
  int ret;

  ret = db_create(&mydb, NULL, 0);
  if (ret != 0) {
    printf("Error creating DB structure!");
    return 1;
  }

  ret = mydb->open(mydb, NULL, "bsddb-py", NULL, DB_HASH, open_flags, 0);
  if (ret != 0) {
    printf("Error opening DB file!");
    return 2;
  }

  mydb->close(mydb, 0);

  return 0;
}
