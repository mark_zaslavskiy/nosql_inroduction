# -*- coding: utf-8 -*-
from pyArango.connection import *


# Подключение
connection = Connection(username="root", password="root")
# Создание БД
if not connection.hasDatabase("testdb"):
    db = connection.createDatabase("testdb")
else:
    db = connection["testdb"]
print db

# Создание коллекции
if not db.hasCollection('students'):
    studentsCollection = db.createCollection(name='students')
else:
    studentsCollection = db.collections['students']

doc = studentsCollection.createDocument()
doc["first_name"] = "Ivan"
doc.save()
doc = studentsCollection.createDocument()
doc["first_name"] = "Fedor"
doc.save()

# Содержимое коллекции
print studentsCollection.fetchAll()

#### AQL

aql = "FOR x IN students RETURN x._key"
dumb_select = db.AQLQuery(aql, rawResults=True, batchSize=100)
print "dumb_select"
print dumb_select

aql1 = "FOR x IN students FILTER x._key == 'Ivan' RETURN x"
select_where = db.AQLQuery(aql1, rawResults=True, batchSize=100)
print "select_where"
print select_where

student = { "first_name": "Natacha", "location": "Antwerp", "tree": {"a":{"b": 1, "c":1}}}
bindVars = {"doc": student}
aql = "INSERT @doc INTO students LET newDoc = NEW RETURN newDoc"
insert_results = db.AQLQuery(aql, bindVars=bindVars)
